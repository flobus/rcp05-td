package tp4;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JLabel; //Import du JLabel qui permet d'afficher du texte
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JPanel;

public class LaFourmiDeLangton extends JFrame {
	//Attributs
	private int dimension = 10;
	JLabel JL = new JLabel("Nombre de tours =0");
	JButton b1 = new JButton("Next");
	JTextField plateau[][] ;

	
	//Constructeur 
	public LaFourmiDeLangton() {
		super();
		this.setTitle("La Fourmi de Langton"); //Donne le titre � l'appli
		this.setSize(500, 500);	//Dimensionne l'appli
		this.setLocation(300, 200);	//Place l'appli sur l'�cran 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	//Permet de terminer l'appli � la fermeture de la fen�tre 
	
		System.out.println("L'application est d�marr�e");
		Container panneau = getContentPane(); //Permet de r�cup�rer la fen�tre
		panneau.add(b1, BorderLayout.SOUTH);
		panneau.add(JL, BorderLayout.NORTH);
		panneau.add(creerPlateau(), BorderLayout.CENTER);
		initFourmi();
		this.setVisible(true);	//Permet de faire afficher l'application 
	}
	//M�thodes
	public JPanel creerPlateau() {
		JPanel p = new JPanel();
		plateau = new JTextField[dimension][dimension];
		p.setLayout(new GridLayout(dimension, dimension));
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				JTextField s = new JTextField();
				plateau[i][j] = s;
				p.add(s);
			}
		}
		//plateau[7][7].setBackground(Color.BLACK);
		return p; 
	}
	
	
	public void initFourmi() {
		Fourmi fourmi = new Fourmi();
		fourmi.x = dimension/2;
		fourmi.y = dimension/2;
		plateau[fourmi.x][fourmi.y].setText("" + fourmi.orientation);
		
	}
	//M�thodes Main
	public static void main(String[] args) {
		LaFourmiDeLangton app = new LaFourmiDeLangton(); //Permet de cr�er l'application 
		app.initFourmi();
	}
}
