package tp3Thread;

public class Compteur1bis implements Runnable {

	private String nom;

	public Compteur1bis(String nom) {
		this.nom=nom;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i =1; i< 1000; i++)
			System.out.print(i + " " + nom );
	}

	public static void  main(String args[]) throws InterruptedException{
		Compteur1 t1, t2, t3;
		t1=new Compteur1("Hello ");	//On met compteur1 dans t1 
		t2=new Compteur1("World ");	//On met compteur1 dans t2
		t3=new Compteur1("and Everybody ");	//On met compteur1 dans t3
		Thread thread1 = new Thread(t1);	//On cr�e un nouveau thread "thread1" dans lequelle on stock le compteur avant de pouvoir executer t1.start
		Thread thread2 = new Thread(t2);	//On cr�e un nouveau thread "thread2" dans lequelle on stock le compteur avant de pouvoir executer t2.start
		Thread thread3 = new Thread(t3);	//On cr�e un nouveau thread "thread3" dans lequelle on stock le compteur avant de pouvoir executer t3.start

		t1.start();
		t2.start();
		t3.start();
		Thread.sleep(100);   //Faire la pause de 100ms

		System.out.println("Fin du programme");
		
		System.exit(0);

	}
}
