package tp3Thread;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class Convertisseur extends JFrame{
	private Container panneau;
	private JButton b1;
	private JTextField jtf1;
	private JTextField jtf2;
	private JTextField jtf3;
	private JLabel jl1;
	private JLabel jl2;
	private JLabel jl3;

	public Convertisseur(){
		//Cr�ation de la fen�tre
		super();
		this.setTitle("Convertisseur");
		this.setSize(500, 500);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();

		panneau.setLayout(new GridLayout(4,3));

		//Cr�ation des objets � utiliser
		jl1 = new JLabel("Montant TVA: ");
		jtf1 = new JTextField("Inscrire taux : ");
		b1 = new JButton("Calculer !"); 
		
		jl2 = new JLabel("Montant HTT ");
		jtf2 = new JTextField("Inscrire taux : ");
		jl3 = new JLabel("Montant TTC : ");
		jtf3 = new JTextField("Inscrire taux : ");
		//Ajout au panneau
		

		panneau.add(jl1);
		panneau.add(jtf1);
		panneau.add(jl2);
		panneau.add(jtf2);
		panneau.add(jl3);
		panneau.add(jtf3);
		//panneau.add(creerPanel(), BorderLayout.NORTH);
		//panneau.add(creerPanel2(), BorderLayout.CENTER);
		panneau.add(b1, BorderLayout.SOUTH);
		setVisible(true);
	}

	/*public JPanel creerPanel(){
		JPanel panel = new JPanel();
		jl1 = new JLabel("Montant TVA: ");
		jtf1 = new JTextField("Inscrire taux : ");
		panel.setLayout(new GridLayout(1,2));

		return panel;	
		}
	
	public JPanel creerPanel2(){
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1,3));
		jl2 = new JLabel("Montant HTT ");
		jtf2 = new JTextField("Inscrire taux : ");
		jl3 = new JLabel("Montant TTC : ");
		return panel2;	
		}	*/
	
	public static void main(String[] args) {
		Convertisseur conv = new Convertisseur();
	}
}

